let b:ale_linters = ['flake8', 'isort', 'mypy']
let b:ale_fixers = ['isort', 'black', 'remove_trailing_lines', 'trim_whitespace']
"let g:ale_python_black_options = '-l 78'
let g:ale_python_mypy_options = '--ignore-imports'
let g:ale_fix_on_save = 1
